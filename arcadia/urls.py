from django.contrib.auth.views import logout_then_login
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index_page, name='index'),
    path('login/', views.login_page, name='login'),
    path('logout/', logout_then_login, name='logout'),

    path('course/<int:course_id>/topic/list/', views.course_topic_list_page, name='course_topic_list'),
    path('topic/<int:topic_id>/view/', views.topic_view_page, name='topic_view'),
]
