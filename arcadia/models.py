import uuid
from datetime import timedelta
from tkinter.messagebox import IGNORE

from django.contrib.auth.models import AbstractUser
from django.db.models import (Model, CharField, DateField, DateTimeField,
                              IntegerField, TextField, UUIDField,
                              ForeignKey, CASCADE, SET_NULL, BooleanField, ManyToManyField)


class User(AbstractUser):
    second_name = CharField('отчество', max_length=30, blank=True, default='')
    timezone = CharField('часовой пояс', max_length=20, default='UTC')

    class Meta:
        verbose_name = 'пользователь'
        verbose_name_plural = 'пользователи'
        permissions = (
            ('teacher', 'преподаватель'),
            ('student', 'студент')
        )

    def __str__(self):
        parts = [self.last_name or self.username]
        if self.first_name:
            parts.append(self.first_name[0] + '.')
        if self.second_name:
            parts.append(self.second_name[0] + '.')
        return ' '.join(parts)


class StudentGroup(Model):
    SPECIALIST = 1
    BACHELOR = 2
    MASTER = 3

    DEGREE = (
        (SPECIALIST, 'специалитет'),
        (BACHELOR, 'бакалавриат'),
        (MASTER, 'магистратура')
    )

    FULL_TIME = 1
    DISTANCE = 2

    FORM = (
        (FULL_TIME, 'очная'),
        (DISTANCE, 'заочная')
    )

    name = CharField(max_length=30, verbose_name='название')
    year = IntegerField('год поступления')
    degree = IntegerField('квалификация', choices=DEGREE)
    form = IntegerField('форма обучения', choices=FORM)
    active = BooleanField('активна', default=False)

    class Meta:
        verbose_name = 'учебная группа'
        verbose_name_plural = 'учебные группы'

    def __str__(self):
        return self.name


class Room(Model):
    name = CharField(max_length=30, verbose_name='название')
    computer_lab = BooleanField('компьютерная', default=False)

    class Meta:
        verbose_name = 'аудитория'
        verbose_name_plural = 'аудитории'

    def __str__(self):
        return self.name


class Semester(Model):
    AUTUMN = 'AUT'
    SPRING = 'SPR'

    SEMESTER = (
        (AUTUMN, 'осенний семестр'),
        (SPRING, 'весенний семестр')
    )

    student_group = ForeignKey('StudentGroup', on_delete=CASCADE, verbose_name='учебная группа')
    year = IntegerField('календарный год')
    semester = CharField('семестр', max_length=3, choices=SEMESTER)
    begin_study = DateField('начало обучения')
    end_study = DateField('конец обучения')
    begin_exams = DateField('начало сессии')
    end_exams = DateField('конец сессии')

    class Meta:
        verbose_name = 'семестр'
        verbose_name_plural = 'график учебного процесса'

    def __str__(self):
        begin = self.year
        end = self.year + 1
        if self.semester == Semester.SPRING:
            begin = self.year - 1
            end = self.year
        return '%s: %d-%d уч.г., %s' % (self.student_group.name, begin, end, self.get_semester_display())

    def study_period(self):
        return '%s — %s' % (self.begin_study, self.end_study)
    study_period.short_description = "Период обучения"

    def exam_period(self):
        return '%s — %s' % (self.begin_exams, self.end_exams)
    exam_period.short_description = "Сессия"


class Course(Model):
    BASIC = 1
    PROFILING = 2
    OPTIONAL = 3
    ELECTIVE = 4

    COURSE_TYPE = (
        (BASIC, 'базовый'),
        (PROFILING, 'обычный'),
        (OPTIONAL, 'курс по выбору'),
        (ELECTIVE, 'факультатив'),
    )

    name = CharField('наименование', max_length=100)
    course_type = IntegerField('тип курса', choices=COURSE_TYPE)
    semester = ForeignKey('Semester', on_delete=IGNORE, verbose_name='семестр')
    lecture_hours = IntegerField('часы лекций', blank=True, default=0)
    lab_work_hours = IntegerField('часы лаб. работ', blank=True, default=0)
    practice_hours = IntegerField('часы практ. работ', blank=True, default=0)
    student_work_hours = IntegerField('часы СРС', blank=True, default=0)
    control_hours = IntegerField('часы КСР', blank=True, default=0)
    total_hours = IntegerField('часов всего', blank=True, default=0)

    class Meta:
        verbose_name = 'дисциплина'
        verbose_name_plural = 'дисциплины'
        ordering = ['name']

    def __str__(self):
        return self.name

    def get_total_hours(self):
        return (self.lecture_hours + self.lab_work_hours + self.practice_hours +
                self.student_work_hours + self.control_hours)


class CourseClass(Model):
    LECTURE = 1
    LAB_WORK = 2
    PRACTICE = 3

    CLASS_TYPES = (
        (LECTURE, 'лекция'),
        (LAB_WORK, 'лабораторная работа'),
        (PRACTICE, 'практическое занятие')
    )

    WEEKDAY = (
        (1, 'понедельник'),
        (2, 'вторник'),
        (3, 'среда'),
        (4, 'четверг'),
        (5, 'пятница'),
        (6, 'суббота'),
        (7, 'воскресенье')
    )

    EVEN_WEEK = 0
    ODD_WEEK = 1
    EVERY_WEEK = 2

    PERIOD = (
        (EVERY_WEEK, 'еженедельно'),
        (ODD_WEEK, 'нечётные недели'),
        (EVEN_WEEK, 'чётные недели')
    )

    NUMBER = (
        (1, '1-я пара'),
        (2, '2-я пара'),
        (3, '3-я пара'),
        (4, '4-я пара'),
        (5, '5-я пара'),
        (6, '6-я пара')
    )

    course = ForeignKey('Course', on_delete=CASCADE, verbose_name='курс')
    class_type = IntegerField('тип занятия', choices=CLASS_TYPES)
    location = ForeignKey('Room', on_delete=SET_NULL, blank=True, null=True, verbose_name='аудитория')
    teacher = ForeignKey('arcadia.User', on_delete=SET_NULL, blank=True, null=True, verbose_name='преподаватель')
    weekday = IntegerField('день недели', choices=WEEKDAY)
    period = IntegerField('период', choices=PERIOD)
    number = IntegerField('номер занятия', choices=NUMBER)

    class Meta:
        verbose_name = 'дисциплина: занятие'
        verbose_name_plural = 'дисциплины: занятия'

    def __str__(self):
        return '%s: %s' % (self.course.name, self.get_class_type_display())

    def get_begin_time(self):
        """ Время начала занятия """
        answer = timedelta()
        if self.number == 1:
            answer = timedelta(hour=8, minute=0)
        if self.number == 1:
            answer = timedelta(hour=9, minute=50)
        if self.number == 1:
            answer = timedelta(hour=11, minute=40)
        if self.number == 1:
            answer = timedelta(hour=14, minute=0)
        if self.number == 1:
            answer = timedelta(hour=15, minute=45)
        if self.number == 1:
            answer = timedelta(hour=17, minute=30)
        return answer

    def get_end_time(self):
        """ Время завершения занятия """
        answer = timedelta()
        if self.number == 1:
            answer = timedelta(hour=9, minute=35)
        if self.number == 1:
            answer = timedelta(hour=11, minute=25)
        if self.number == 1:
            answer = timedelta(hour=13, minute=15)
        if self.number == 1:
            answer = timedelta(hour=15, minute=35)
        if self.number == 1:
            answer = timedelta(hour=17, minute=20)
        if self.number == 1:
            answer = timedelta(hour=19, minute=5)
        return answer

    def get_first_class_date(self):
        """ Дата первого занятия в семестре """
        semester = self.course.semester
        _, study_week_number, study_week_day = semester.begin_study.isocalendar()

        # Сколько дней нужно добавить к началу семестра
        days = self.weekday - study_week_number
        if days < 0:
            days += 7

        # Если чётность недель несовпадает - добавим ещё неделю
        if self.period != self.EVERY_WEEK:
            if study_week_number % 2 != self.period:
                days += 7

        return semester.begin_study + timedelta(days=days)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        date = self.get_first_class_date()

        event = Event()
        event.summary = event.description = '%s, %s' % (self.course.name, self.get_class_type_display())
        event.begin = date + self.get_begin_time()
        event.end = date + self.get_end_time()
        event.location = self.location
        event.period = Event.WEEKLY
        event.interval = 1 if self.period == self.EVERY_WEEK else 2
        event.save()

        event.participant_set.add(self.teacher)


class CourseElement(Model):
    kind = 'element'

    course = ForeignKey('Course', on_delete=CASCADE, verbose_name='курс обучения')
    code = CharField('код', max_length=10, default='')
    name = CharField('название', max_length=150, default='')
    modified = DateTimeField('дата и время изменения', auto_now=True)

    class Meta:
        abstract = True
        ordering = ['course', 'code']


class CourseTopic(CourseElement):
    kind = 'topic'

    text = TextField('текст в формате Markdown', blank=True, default='')

    class Meta:
        verbose_name = 'дисциплина: теоретический материал'
        verbose_name_plural = 'дисциплины: теоретические материалы'

    def __str__(self):
        return '%s: %s %s' % (self.course.name, self.code, self.name)


class Event(Model):
    SECONDLY = 1
    MINUTELY = 2
    HOURLY = 3
    DAILY = 4
    WEEKLY = 5
    MONTHLY = 6
    YEARLY = 7

    FREQUENCY = (
        (SECONDLY, 'посекундно'),
        (MINUTELY, 'поминутно'),
        (HOURLY, 'ежечасно'),
        (DAILY, 'ежедневно'),
        (WEEKLY, 'еженедельно'),
        (MONTHLY, 'ежемесячно'),
        (YEARLY, 'ежегодно'),
    )

    summary = CharField(max_length=100, verbose_name='название')
    begin = DateTimeField('дата и время начала')
    end = DateTimeField('дата и время завершения')
    location = ForeignKey('Room', on_delete=CASCADE, verbose_name='Аудитория')
    period = IntegerField('периодичность', choices=FREQUENCY, blank=True)
    interval = IntegerField('интервал', blank=True, default=0)
    description = TextField('описание', blank=True, default='')
    participant = ManyToManyField('User', verbose_name='участники')
    modified = DateTimeField('дата и время редактирования', auto_now=True)
    uuid = UUIDField('уникальный идентификатор')

    class Meta:
        verbose_name = 'событие'
        verbose_name_plural = 'события'

    def __str__(self):
        return self.summary

    def save(self, *args, **kwargs):
        if not self.uuid:
            self.uuid = uuid.uuid4()
        super().save(*args, **kwargs)
