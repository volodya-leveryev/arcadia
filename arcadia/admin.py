from django.contrib import admin

from . import models


@admin.register(models.StudentGroup)
class StudentGroupAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False
    list_display = ['name', 'year', 'degree', 'form', 'active']
    list_filter = ['year', 'degree', 'form', 'active']
    search_fields = ['name']


@admin.register(models.Room)
class RoomAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False
    list_display = ['name', 'computer_lab']
    list_filter = ['computer_lab']
    search_fields = ['name']


@admin.register(models.Semester)
class SemesterAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False
    list_display = ['student_group', 'year', 'semester', 'study_period', 'exam_period']
    list_filter = ['student_group', 'year', 'semester']


@admin.register(models.Course)
class CourseAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False
    list_display = ['name', 'semester', 'course_type', 'lecture_hours', 'practice_hours', 'lab_work_hours',
                    'total_hours']
    list_filter = ['course_type', 'semester']
    search_fields = ['name']


@admin.register(models.CourseClass)
class CourseClassAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False
    list_display = ['course', 'class_type', 'location', 'teacher', 'weekday', 'number', 'period']
    list_filter = ['course', 'teacher', 'class_type', 'location', 'weekday', 'number']
    search_fields = ['course']


@admin.register(models.CourseTopic)
class CourseTopicAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False
    list_display = ['course', 'code', 'name']
    list_filter = ['course']


@admin.register(models.User)
class UserAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False
    list_display = ['username', 'last_name', 'first_name', 'second_name', 'email']
    list_display_links = ['username', 'last_name', 'first_name', 'second_name', 'email']
    search_fields = ['username', 'last_name', 'first_name', 'second_name', 'email']


admin.site.register(models.Event)

admin.site.site_header = 'Администрирование'
admin.site.site_title = 'Администрирование'
admin.site.index_title = 'Таблицы'
