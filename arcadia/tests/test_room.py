from django.test import TestCase, Client
from django.urls import reverse_lazy

from arcadia import models


class TestRoom(TestCase):
    def setUp(self):
        user = models.User()
        user.username = 'user'
        user.is_staff = True
        user.set_password('password')
        user.save()

    def test_list_page(self):
        c = Client()
        c.login(username='user', password='password')
        r = c.get(reverse_lazy('room_list'))
        self.assertContains(r, 'Аудитории', status_code=200)
        self.assertContains(r, 'Аудитории ещё не созданы', status_code=200)
        self.assertContains(r, 'Создать')

    def test_create_page(self):
        c = Client()
        c.login(username='user', password='password')
        r = c.get(reverse_lazy('room_create'))
        self.assertContains(r, 'Редактирование аудитории', status_code=200)
        self.assertContains(r, 'Сохранить')

    def test_delete_page(self):
        c = Client()
        c.login(username='user', password='password')
        # r = c.get(reverse_lazy('room_delete'))
        # self.assertContains(r, 'Редактирование аудитории', status_code=200)
        # self.assertContains(r, 'Сохранить')
