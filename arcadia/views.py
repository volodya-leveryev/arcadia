from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.forms import (CharField, Form)
from django.forms.widgets import (PasswordInput)
from django.shortcuts import render, redirect, get_object_or_404

from .models import (Course, CourseTopic)


class LoginForm(Form):
    """ Форма входа в систему """
    username = CharField(max_length=150, label='Логин')
    password = CharField(widget=PasswordInput(), label='Пароль')


def login_page(request):
    """ Страница входа в систему """
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(**form.cleaned_data)
            if user is None:
                form.add_error(None, 'Неправильный логин или пароль')
            else:
                login(request, user)
                return redirect('index')
    else:
        form = LoginForm()
    return render(request, 'arcadia/login.html', {'form': form})


def index_page(request):
    object_list = Course.objects.all()
    return render(request, 'arcadia/index.html', {
        'object_list': object_list
    })


@login_required
def course_topic_list_page(request, course_id):
    course = get_object_or_404(Course, id=course_id)
    course_topic_list = CourseTopic.objects.filter(course_id=course_id)
    return render(request, 'arcadia/course_topic_list.html', {
        'course': course,
        'object_list': course_topic_list
    })


@login_required
def topic_view_page(request, topic_id):
    topic = get_object_or_404(CourseTopic, id=topic_id)
    course = topic.course
    return render(request, 'arcadia/course_topic.html', {
        'course': course,
        'topic': topic
    })
