from django.apps import AppConfig


class ArcadiaConfig(AppConfig):
    name = 'arcadia'
    verbose_name = 'Проект Аркадия'
